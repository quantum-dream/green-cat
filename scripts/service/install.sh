#!/bin/bash
SYSTEMD=/etc/systemd/system
WD=$(dirname $0)

for file in $(ls $WD/*.service)
do
	name=$(basename $file)
	cp $WD/$name $SYSTEMD
	chmod +x $SYSTEMD/$name
	systemctl enable $name
	systemctl start $name
done
systemctl daemon-reload
