#!/bin/bash
SYSTEMD=/etc/systemd/system
WD=$(dirname $0)

for file in $(ls $WD/*.service)
do
	name=$(basename $file)
	systemctl stop $name
	systemctl disable $name
	rm -f $SYSTEMD/$name
done
systemctl daemon-reload
