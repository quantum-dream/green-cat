/*
green-cat
Copyright (C) 2017  Quantum Dream Studio

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * Green Cat application main module.
 */
(function (global) {
	"use strict";
	// Global variables
	/** AngularJS. */
	var angular = global.angular;
	/** Material Design Lite. */
	var mdl = global.componentHandler;
	/** Browser History API. */
	var history = global.history;

	/** Tte application instance. */
	var app = angular.module('GreenCat', ['ngRoute']);

	/**
	 * Configures the application.
	 */
	app.config(function ($routeProvider, $logProvider) {
		$logProvider.debugEnabled(true);
		$routeProvider
			.when('/', {
				templateUrl: '/static/templates/screen.main.html',
				controller: 'SensorsCtrl'
			})
			.when('/settings', {
				templateUrl: '/static/templates/screen.settings.html',
				controller: 'SettingsCtrl'
			});
	});
	/**
	 * Initializes the application.
	 */
	app.run(function ($rootScope, $location, backend) {
		// Loading the settings
		backend.getSettings().then(function (settings) {
			$rootScope.settings = settings;
		});
		/** MDL requires upgrade of the dynamically loaded elements. */
		$rootScope.$on('$viewContentLoaded', function (e) {
			$rootScope.mdlInitialize();
		});
		/**
		 * Initialized MDL components.
		 */
		$rootScope.mdlInitialize = function () {
			componentHandler.upgradeAllRegistered();
		};
		/**
		 * Shows application-level message.
		 * @param  {String} message
		 *         the message text to be shown
		 * @param  {Number} timeout
		 *         the time in milliseconds for which the message appers on screen
		 */
		$rootScope.showAppMessage = function (message, timeout) {
			var snackbar = document.getElementById('messages');
			snackbar.MaterialSnackbar.showSnackbar({
				message: message,
				timeout: timeout || 5000
			});
		};
		/**
		 * Goes back in session history.
		 */
		$rootScope.back = function () {
			history.back();
		};
		// Completing initialization
		$rootScope.mdlInitialize();
	});

	/**
	 * Controller of the main screen.
	 */
	app.controller('SensorsCtrl', function ($scope, $timeout, $log, backend) {
		/**
		 * Starts automatic refresh of the sensors values.
		 */
		function autoRefresh() {
			backend.getSensors().then(function (sensors) {
				$scope.sensors = sensors;
				$scope._refresher = $timeout(autoRefresh,
					$scope.settings ? $scope.settings.sensorsReadIntervalSec * 1000 : 1000);
			});
		}
		/**
		 * Stops automatic refresh of the sensors values.
		 */
		function stopRefresh() {
			if ($scope._refresher) {
				$timeout.cancel($scope._refresher);
				delete $scope._refresher;
			}
		}
		// Registering event handlers
		/**
		 * Controller is being initialized.
		 */
		$scope.$on('$viewContentLoaded', function (e) {
			$log.debug('Sensors view activated');
			stopRefresh();
			autoRefresh();
		});
		/**
		 * Controller is being destroyed.
		 */
		$scope.$on('$destroy', function (e) {
			stopRefresh();
			$log.debug('Sensors view deactivated');
		});
	});

	/**
	 * Controller of the settings screen.
	 */
	app.controller('SettingsCtrl', function ($scope, $rootScope, $log, backend) {
		$scope.timePattern = new RegExp('\\d{2}:\\d{2}');
		// Registering event handlers
		/**
		 * Controller is being initialized.
		 */
		$scope.$on('$viewContentLoaded', function (e) {
			$log.debug('Settings view activated');
			// Loading the most recent settings
			backend.getSettings().then(function (settings) {
				$scope.settings = settings;
				// Synchronizing MDL
				document.getElementById('quietMode_on').parentElement.MaterialCheckbox.checkToggleState();
			});
		});
		/**
		 * Controller is being destroyed.
		 */
		$scope.$on('$destroy', function (e) {
			$log.debug('Settings view deactivated');
		});
		/**
		 * Save the settings.
		 */
		$scope.save = function () {
			backend.setSettings($scope.settings).then(function (settings) {
				$scope.settings = $rootScope.settings = settings;
			});
		};
	});

	/**
	 * Custom component that renders a card with sensor details.
	 */
	app.component('sensorCard', {
		templateUrl: '/static/templates/directive.sensor-card.html',
		bindings: {
			name: '@',
			value: '@'
		}
	});

	/**
	 * Service for communication with the backend.
	 */
	app.service('backend', function ($http, $rootScope) {
		/**
		 * @return {Promise} the last sensors readings
		 */
		this.getSensors = function () {
			return $http.get('/sensors')
				.then(function (response) {
					return response.data;
				})
				.catch(function () {
					$rootScope.showAppMessage('Не удалось получить показания датчиков');
				});
		};
		/**
		 * @return {Promise} the current application settings
		 */
		this.getSettings = function () {
			return $http.get('/settings')
				.then(function (response) {
					return response.data;
				})
				.catch(function () {
					$rootScope.showAppMessage('Не удалось получить настройки приложения');
				});
		};
		/**
		 * Updates the application settings. Partial update supported.
		 * @param  {Object} settings the settings to be updated
		 * @return {Promise} the current application settings after update
		 */
		this.setSettings = function (settings) {
			return $http.put('/settings', settings)
				.then(function (response) {
					return response.data;
				})
				.catch(function () {
					$rootScope.showAppMessage('Не удалось обновить настройки приложения');
				});
		};
	});

}(window));
