"""
green-cat
Copyright (C) 2017  Quantum Dream Studio

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import logging
import random
import time

from injector import inject

from app.config import Config
from . import GreenCat


class FakeCat(GreenCat):
	"""
	Fake implementation of the `GreenCat` interface to be used for local testing.
	"""

	@inject
	def __init__(self, cfg: Config):
		self.__logger = logging.getLogger(type(self).__name__)
		self.__logger.info('Settings: %s', cfg.settings)

	@property
	def sensor_soil_humidity(self) -> int:
		return random.randint(0, 100)

	@property
	def sensor_cpu_temperature(self) -> int:
		return random.randint(0, 100)

	@property
	def sensor_wifi_signal(self) -> int:
		return random.randint(-80, 0)

	def play_mp3(self, file_path: str):
		self.__logger.info('Pretending playing back file %s', file_path)
		# Just block fo 30 seconds
		time.sleep(10)
		return True
