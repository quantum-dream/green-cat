"""
green-cat
Copyright (C) 2017  Quantum Dream Studio

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import abc
import datetime
import random
import tzlocal

from glob import glob
from os import path


class GreenCat(metaclass=abc.ABCMeta):
	"""
	Interface class for accessing GreenCat device capabilities.
	"""
	__SENSOR_ACCESSOR_PREFIX = 'sensor_'

	@property
	def sensors(self) -> dict:
		"""
		Build a dictionary of the sensors readings provided by this `GreenCat` instance.
		This method finds all the members (properties or getters) of the instance
		with names starting with `sensor_` prefix, extracts the sensor name by striping
		the prefix away and gets the values from the instance.

		:return: the dictionary of names and values of every known sensor
		"""
		values = {
			'__ts': datetime.datetime.now(tzlocal.get_localzone()).isoformat()
		}
		for name in dir(self):
			# Iterating over all the members of this instance
			if name.startswith(GreenCat.__SENSOR_ACCESSOR_PREFIX):
				sensor = name[len(GreenCat.__SENSOR_ACCESSOR_PREFIX):]
				# The member with this name is a sensor reading accessor
				accessor = getattr(type(self), name)
				if isinstance(accessor, property):
					# Accessor if a property
					values[sensor] = getattr(self, name)
				elif callable(accessor):
					# Accessor is a method
					values[sensor] = accessor(self)
				else:
					# Just an attribute. Kinda useless, but anyway
					values[sensor] = getattr(self, name)
		return values

	@property
	@abc.abstractmethod
	def sensor_wifi_signal(self) -> int:
		"""
		:return: the current level of WiFi signal on the device in decibels
		"""
		pass

	@property
	@abc.abstractmethod
	def sensor_cpu_temperature(self) -> int:
		"""
		:return: the current CPU temperature on the device in degrees of Celsius
		"""
		pass

	@property
	@abc.abstractmethod
	def sensor_soil_humidity(self) -> int:
		"""
		:return: the reading of the soil humidity sensor in percents
		"""
		pass

	@abc.abstractmethod
	def play_mp3(self, file_path: str) -> bool:
		"""
		Play a file by a given path as an MP3-encoded audio. Blocks until the playback if complete.

		:param file_path:
			the path to the file
		:return:
			`True` if the audio file has been played back successfully;
			`False` if either the file does not exist, or is not a valid MP3-encoded audio file
		"""
		pass

	def play_random_mp3(self, directory_path: str) -> bool:
		"""
		Plays one random MP3-encoded audio file from a given directory.

		:param directory_path:
			the path to a directory containing MP3 files
		:return:
			`True` if the random audio file has been selected and played back successfully;
			`False` if either there are no files in the given directory, or the randomly selected file
			was not a valid MP3-encoded audio file
		"""
		mp3_files = glob(path.join(path.abspath(directory_path), '*.mp3'), recursive=False)
		if mp3_files:
			# A file can be selected
			selected_file = random.choice(mp3_files)
			return self.play_mp3(selected_file)
		else:
			# No MP3 files in the directory
			return False
