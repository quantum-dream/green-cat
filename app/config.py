"""
green-cat
Copyright (C) 2017  Quantum Dream Studio

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import json


class Config:
	# Turn on/off debug mode (more messages)
	debug = True
	# Port of the HTTP server
	http_port = 8080
	# Name of a wireless network interface
	wifi_interface = 'wlan0'
	# COM port to be used for communication with the sensors module
	com_port = '/dev/ttyS2'
	# The application settings
	settings_file = './settings.json'

	def __init__(self):
		self.__settings = None

	@property
	def settings(self) -> dict:
		"""
		:return: the application settings loaded from file
		"""
		if not self.__settings:
			with open(self.settings_file) as f:
				self.__settings = json.load(f)
		return self.__settings

	@settings.setter
	def settings(self, update: dict):
		"""
		Update the application settings.

		:param update:
			the updated settings (may be partial)
		"""
		self.dict_merge(self.__settings, update)
		with open(self.settings_file, mode='w') as f:
			json.dump(self.__settings, f, sort_keys=True, indent='\t')

	@property
	def green_cat_impl(self):
		"""
		:return: the implementation of `GreenCat` interface to be used across the application
		"""
		from app.device.real import RealGreenCat
		return RealGreenCat

	@staticmethod
	def dict_merge(target: dict, other: dict):
		"""
		Recursively merge a dictionary into a given target.

		:param target:
			the dictionary that gets updated
		:param other:
			the dictionary which data will be merged into the target
		"""
		for k, v in other.items():
			if k in target:
				if isinstance(target[k], dict) and isinstance(v, dict):
					Config.dict_merge(target[k], v)
				else:
					target[k] = v
